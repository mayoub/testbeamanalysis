/*
This macro contains some parameters that are constant for all the other macros. It also contains some functions that can be used by other macros. To be loaded (.L Common.C) before running the other macros on root-5 or include it header-like in root-6
*/
#include "TCanvas.h"
#include "TMath.h"
#include "TStyle.h"
#include "TROOT.h"


//-------------------------------------------------------------------------------------------------------------------------------------//
TString cTreeNameInInputFile = "tree";
TString cDataPath            = "../../testbeamanalysis/Data";
// TString cDataPath            = "/eos/user/m/mayoub/Postdoc/TestBeamFiles/LGA35_Study/May2019";
TString cHistoResultsPath = "ResultsHistos";
TString cPDFResultsPath   = "ResultsPlots";
//-------------------------------------------------------------------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------------------------------------------------//
const Int_t cNumberOfChannels = 8;
const Int_t cNumberOfIntBranches   = 6;
const Int_t cNumberOfFloatBranches   = 27;
const Int_t cNumberOfDoubleBranches = 4;

const TString cTreeIntBranches[cNumberOfIntBranches] = {"FEI4x", "FEI4y", "runNumber", "eventNumber", "cycleNumber", "nChannel"};
const TString cTreeFloatBranches[cNumberOfFloatBranches]  = {"pulseHeightNoise", "pulseHeight", "timeCFD", "timeZCD", "timeCFD20", "timeCFD70", "timeZCD20", "timeCFD50", "timeZCD50", "timeCTD", "timeAtMax", "pedestal", "noise", "noiseZCD", "charge", "jitter", "jitterCFD20", "jitterCFD50", "jitterCTD", "riseTime1090", "derivative", "derivativeCFD20", "derivativeCFD50", "derivativeCTD", "derivativeZCD", "width", "TOT"};
enum enumFloatVarName{kpulseHeightNoise, kpulseHeight, ktimeCFD, ktimeZCD, ktimeCFD20, ktimeCFD70, ktimeZCD20, ktimeCFD50, ktimeZCD50, ktimeCTD, ktimeAtMax, kpedestal, knoise, knoiseZCD, kcharge, kjitter, kjitterCFD20, kjitterCFD50, kjitterCTD, kriseTime1090, kderivative, kderivativeCFD20, kderivativeCFD50, kderivativeCTD, kderivativeZCD, kwidth, kTOT};
const TString cTreeDoubleBranches[cNumberOfDoubleBranches]  = {"Xtr","Ytr","DeltaXtr","DeltaYtr"};
enum enumDoubleVarName{kXtr, kYtr, kDeltaXtr, kYDeltaTr};

//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//Define here a double array on int containing the index of variable you want to produce delta histograms for:
//an element of the array is an array like that {variableIndex, channel1, channel2};
const Int_t arrayDeltaHistograms[][3] = {
  {ktimeCFD20, 0, 3},
  {ktimeCFD20, 0, 1},
  {ktimeCFD20, 1, 3},
  {ktimeAtMax, 0, 3}

};
const Int_t cNumberOfDeltaHistograms = sizeof( arrayDeltaHistograms ) / sizeof( arrayDeltaHistograms[0] );

//-------------------------------------------------------------------------------------------------------------------------------------//
Bool_t cApplyDeltaCut = kTRUE;
Int_t cDeltaCutVariable = ktimeAtMax;
Int_t cDeltaCutFirstChannel = 0;
Int_t cDeltaCutSecondChannel = 3;

//April
//Double_t cDeltaCutMin = -3700;
//Double_t cDeltaCutMax = -3550;
Double_t cutCenterValue = 7310.0000;
Double_t cutWindow = 1000;

//Double_t cDeltaCutMin = -3500;
//Double_t cDeltaCutMax = -3300;


//-------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------------------------------------------------//
Bool_t cApplyNoiseCut = kFALSE;
Int_t cNoiseCutVariables[2] = {kpulseHeight, knoise};
Double_t cNoiseCutValue = 5;
Int_t cNoiseCutChannel = 0;
//-------------------------------------------------------------------------------------------------------------------------------------//

//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//*************************************************************************************************************************************//
//-------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------------------------------------------------//
//Declare here the bining (min,max,and nBins) of your histograms. each triplet corresponds to the variable with the same index in cTreeFloatBranches (Is this english !!!!!!?).
enum enumBining{kNBins,kMinBin,kMaxBin};
const Float_t cHistogramsBins[cNumberOfFloatBranches][3] = {
  {1200,-100,300},//pulseHeightNoise
  {1000,0,1000},////pulseHeight
  {11000,0,220000},//timeCFD
  {11000,0,220000},//timeZCD
  {11000,0,220000},//timeCFD20
  {11000,0,220000},//timeCFD70
  {11000,0,220000},//timeZCD20
  {11000,0,220000},//timeCFD50
  {11000,0,220000},//timeZCD50
  {11000,0,220000},//timeCTD
  {11000,0,220000},//timeAtMax
  {2000,-500,500},//pedestal
  {3000,0,300},//noise
  {3000,0,300},//noiseZCD
  //{10000,-500000,3000000},//charge
  {10000,-100,100},//charge
  {10000,-500000,3000000},//jitter
  {10000,-500000,3000000},//jitterCFD20
  {10000,-500000,3000000},//jitterCFD50
  {10000,-500000,3000000},//jitterCTD
  {10000,0,10000},//riseTime1090
  {1000,-2,2},//derivative
  {1000,-2,2},//derivativeCFD20
  {1000,-2,2},//derivativeCFD50
  {1000,-2,2},//derivativeCTD
  {1000,0,1000},//derivativeZCD
  {1000,0,1000},//width
  {10000,-300000,50000}//TOT
};
//For the X,Y 2-D histograms, no need to use an array:
Int_t cNumberOfXBins = 100;
Double_t cXBinMin = 4.5;
Double_t cXBinMax = 9;
Int_t cNumberOfYBins = 100;
Double_t cYBinMin = 3.5;
Double_t cYBinMax = 7.5;
//-------------------------------------------------------------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------------------------------------------------//
//Array of colors to be picked when drawing histograms on the same pad.
Int_t cColorVsChannel[cNumberOfChannels] = {kBlack, kBlue, kRed, kMagenta, kGreen+2, kOrange+1, kGray+2, kCyan};
//-------------------------------------------------------------------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------------------------------------------------//
Bool_t cVerboseOption = kFALSE;
//-------------------------------------------------------------------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------------------------------------------------//
void SetHistoStyle(TH1F *histoSomething, Int_t color, Int_t markerStyle, Float_t markerSize, TString xAxisTitle, TString yAxisTitle){
  histoSomething->SetLineColor(color);
  histoSomething->SetMarkerColor(color);
  histoSomething->SetMarkerStyle(markerStyle);
  histoSomething->SetMarkerSize(markerSize);
  histoSomething->GetXaxis()->SetLabelSize(0.025);
  histoSomething->GetXaxis()->SetTitleSize(0.035);
  histoSomething->GetXaxis()->SetTitleOffset(1);
  histoSomething->GetXaxis()->SetTitle(xAxisTitle);
  histoSomething->GetYaxis()->SetLabelSize(0.035);
  histoSomething->GetYaxis()->SetTitleSize(0.035);
  histoSomething->GetYaxis()->SetTitleOffset(1.35);
  histoSomething->GetYaxis()->SetTitle(yAxisTitle);
}

//Set the style of the canvas. Some repetitions are to be cleared.
void SetCanvasStyle(TCanvas *can){
  // gStyle->SetOptStat(0);
  int font = 42;
  // gROOT->SetStyle("Plain");
  gStyle->SetTitleFillColor(0);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetLegendBorderSize(1);
  gStyle->SetTextFont(font);
  gStyle->SetTickLength(0.02,"y");
  gStyle->SetTickLength(0.02,"x");
  can->SetTickx();
  can->SetTicky();
  gStyle->SetLabelSize(0.04,"xyz");
  gStyle->SetLabelFont(font,"xyz");
  gStyle->SetLabelOffset(0.01,"xyz");
  gStyle->SetTitleFont(font,"xyz");
  gStyle->SetTitleOffset(1.1,"xy");
  gStyle->SetTitleSize(0.04,"xyz");
  gStyle->SetMarkerSize(1.1);
  gStyle->SetLineWidth(1);
  gStyle->SetLegendFont(42);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(10);
  gStyle->SetEndErrorSize(0);
  can->SetFillColor(0);
  can->SetBorderMode(0);
  can->SetBorderSize(0);
  can->SetLeftMargin(0.09);
  can->SetRightMargin(0.11);
  can->SetBottomMargin(0.1518219);
  // can->SetTopMargin(0.);
  can->SetFrameBorderMode(0);
}
//-------------------------------------------------------------------------------------------------------------------------------------//
void CommonParams(){
}
//-------------------------------------------------------------------------------------------------------------------------------------//

#!/bin/bash
#The supported options: Fill, draw, or fit
optList="hfdas"
fillHistograms=0
drawPlots=0
darwAllChannels=0
drawAllSeparetly=0
fitDistribution=0
EXIT=0
while getopts $optList option
do
  case $option in
    h ) fillHistograms=1;;
    d ) drawPlots=1;;
    a ) darwAllChannels=1;;
    s ) drawAllSeparetly=1;;
    f ) fitDistribution=1;;
    * ) echo "Unimplemented option chosen."
    EXIT=1
    ;;
  esac
done

shift $(($OPTIND - 1))

if [ $EXIT -eq 1 ]; then
  echo "Usage: `basename $0` (-$optList)"
  echo "       -h  Fill the histograms from the tree for a given configuration"
  echo "       -d  Draw a variable for all the channels or just for one of them given as argument"
  echo "       -a  When d option is on, draw all the channels"
  echo "       -s  When a,d options are on, draw all the channels separetly"
  echo "       -f  Fit the charge distribution of a given charge distribution (configuration and channel as arguments)"
  exit 4
fi

#Export the arguments from the user
CONFIG_NAME=""
VAR_NAME=""
CHANNEL_NUMBER=""

while [ ! -z "$1" ]; do
  arg="$1"
  shift

  if [ "$arg" = "++config" ]; then
    CONFIG_NAME="$1"
    shift
  elif [ "$arg" = "++channel" ]; then
    CHANNEL_NUMBER="$1"
    shift
  elif [ "$arg" = "++var" ]; then
    VAR_NAME="$1"
    shift
  fi
done

export CONFIG_NAME CHANNEL_NUMBER VAR_NAME

#Abort if root is not found
# echo "Checking if root is accessible:"
# which root.exe
# if [ $? -ne 0 ]; then
#   echo "Root not found. Please make sure to have it in the path"
#   exit
# fi

#define some common values and mkdir directories for results
ResultsHistoDir="ResultsHistos"
ResultsPlotDir="ResultsPlots"

mkdir $ResultsHistoDir
mkdir $ResultsPlotDir

if [ $fillHistograms -eq 1 ]; then
  root -l -b -q "FillHistograms.C+(\"$CONFIG_NAME\")"
fi

if [ $drawPlots -eq 1 ]; then
  if [ $darwAllChannels -eq 1 ]; then
    if [ $drawAllSeparetly -eq 1 ]; then
    root -l -b -q "DrawHistograms.C+(\"$CONFIG_NAME\", \"$VAR_NAME\",-1,1,1)"
  else
    root -l -b -q "DrawHistograms.C+(\"$CONFIG_NAME\", \"$VAR_NAME\",-1,1,0)"
  fi
  else
    root -l -b -q "DrawHistograms.C+(\"$CONFIG_NAME\", \"$VAR_NAME\",$CHANNEL_NUMBER,0,0)"
  fi
fi

if [ $fitDistribution -eq 1 ]; then
  root -l -b -q "FitDistribution.C+(\"$CONFIG_NAME\", \"$VAR_NAME\", $CHANNEL_NUMBER)"
fi


exit 0

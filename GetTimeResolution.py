#!/usr/bin/env python

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Import Modules                                                             #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
import sys, os, string, shutil,pickle, subprocess, math, getpass
import numpy as np
from numpy.linalg import inv



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Root                                                                       #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
import ROOT
from ROOT import *
gStyle.SetOptStat(0)
gROOT.SetBatch(True)

if getpass.getuser()=="makovec":
    ROOT.gROOT.LoadMacro("~/Scripts/atlasstyle/AtlasStyle.C");
    ROOT.SetAtlasStyle()

gStyle.SetPalette(1)
gStyle.SetOptFit(1111)
gStyle.SetOptStat(1111)

ROOT.gErrorIgnoreLevel=ROOT.kWarning


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#                                                                            #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

#chi2 minimization to extract the time resolution of several devices based on DeltaT measurements.
#inputMat is a matrix containing the width of the deltaT(i,j) distributions
#inputErMat is a matrix containing the error on the width of the deltaT(i,j) distributions
def GetTimingResolution(inputMat,inputErMat):

  #take the square and propagate uncertainties
  betaMat=np.power(inputMat,2)
  betaErMat=2*inputMat*inputErMat


  N=betaMat.shape[0]
  matrixU=np.zeros(betaMat.shape)
  tvectorA=np.zeros(N)
  tvectorB=np.zeros(N)

  #compute B
  for ie1 in range(0,N):
    tvectorB[ie1]=0;
    for ie2 in range(0,N):
      if(betaErMat[ie1,ie2]!=0):
        tvectorB[ie1]+=betaMat[ie1,ie2]/(betaErMat[ie1,ie2]*betaErMat[ie1,ie2])

  # #coupute U
  for ie1 in range(0,N):
    for ie2 in range(0,N):
      matrixU[ie1,ie2]=0;
      if(ie1==ie2):
        for k in range(0,N):
          if betaErMat[ie1][k]!=0:
            matrixU[ie1][ie2]+= 1/(betaErMat[ie1][k]*betaErMat[ie1][k]);
      elif betaErMat[ie1][ie2]!=0:
        matrixU[ie1][ie2]= 1/(betaErMat[ie1][ie2]*betaErMat[ie1][ie2]);
      matrixU[ie2][ie1]=matrixU[ie1][ie2];


  # #inversion
  uinv=inv(matrixU)
  a=np.dot(uinv,tvectorB)
  aer=np.sqrt(np.diag(uinv))

  #take the squareroot and propagate uncertainties
  print a
  output=np.sqrt(a)
  outputEr=(1/2.)*np.power(a,-1/2.)*aer

  #remove nan
  for i in range(len(output)):
      if np.isnan(output[i]):output[i]=0
  for i in range(len(outputEr)):
      if np.isnan(outputEr[i]):outputEr[i]=0

  return(output,outputEr)




#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
#                                                                            #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

inputMat = np.loadtxt('Sigmas_data_September2018.txt', delimiter=' ')
inputErMat = np.loadtxt('SigmasErr_data_September2018.txt', delimiter=' ')


print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print inputMat
print inputErMat
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"


#Compute the time resolution
(reso,resoEr)=GetTimingResolution(inputMat,inputErMat)

for i in range(len(reso)):
    print "sigma_t",i,reso[i],'+/-',resoEr[i]

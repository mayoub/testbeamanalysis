//This macro is only for drawing of the hitmpas. It take the output of the previous macro (MakeHitsAndClusters.C).
//needed root classes
#include "Riostream.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TString.h"
#include "TMath.h"
#include "TStyle.h"
#include "TDirectory.h"
#include "TGraphErrors.h"

// //needed macros
#include "CommonParams.C"

//The main function
void FillHistograms(TString configName = "data_1539916005.dat")
{
  // gStyle->SetOptStat(0);

  //-------------------------------------------------------------------------------------------------------------------------------------//
  //This is the input file containing the tree for each configuration (merged runs)
  TFile *inputFile = new TFile(Form("%s/%s.root",cDataPath.Data(),configName.Data()));
  if(!inputFile){
    cout<<Form("!!!!!!!!!!!!!!!!!!!! File for the configuration %s is not found.",configName.Data());
    return;
  }
  TTree *testBeamEventTree = ((TTree*) inputFile->Get(cTreeNameInInputFile));
  // testBeamEventTree->SetBranchAddress("event", &event);
  Long_t numberOfEntries = testBeamEventTree->GetEntries();
  //-------------------------------------------------------------------------------------------------------------------------------------//


  //-------------------------------------------------------------------------------------------------------------------------------------//
  //Define an array of integer in order to get the integer branches of the tree. This can be done in a more intelligent way if you just want to plot them but this way you are saving the data for later use..
  Int_t arrayIntBranches[cNumberOfIntBranches];
  for(Int_t iBranch=0;iBranch<cNumberOfIntBranches;iBranch++){
    testBeamEventTree->SetBranchAddress(cTreeIntBranches[iBranch], &arrayIntBranches[iBranch]);
  }

  //Similarly define an array of floats in order to get the float branches of the tree. The floats themselves are arrays -> 2d arrays..
  Float_t arrayFloatBranches[cNumberOfFloatBranches][cNumberOfChannels];
  for(Int_t iBranch=0;iBranch<cNumberOfFloatBranches;iBranch++){
    testBeamEventTree->SetBranchAddress(cTreeFloatBranches[iBranch], &arrayFloatBranches[iBranch]);
  }

  //Similarly define an array of double in order to get the float branches of the tree. (XY)
  Double_t arrayDoubleBranches[cNumberOfDoubleBranches][cNumberOfChannels];
  for(Int_t iBranch=0;iBranch<cNumberOfDoubleBranches;iBranch++){
    testBeamEventTree->SetBranchAddress(cTreeDoubleBranches[iBranch], &arrayDoubleBranches[iBranch]);
  }

  //Drop this variable out of the loop since it is a float and not in an array. //This is stupid, fix it after!!!!!!
  Float_t eventTime;
  testBeamEventTree->SetBranchAddress("eventTime", &eventTime);
  //-------------------------------------------------------------------------------------------------------------------------------------//


  //-------------------------------------------------------------------------------------------------------------------------------------//
  //Here define the histograms. For the moment, only 1D histograms for each array entry is defined. Correlations histograms (nD) can be added after similarly
  //The bining of the histograms is defined in the external macro CommonParams.C
  TH1F ***histoVariablesPerChannel = new TH1F**[cNumberOfFloatBranches];
  for(Int_t iBranch =0;iBranch<cNumberOfFloatBranches;iBranch++){
    histoVariablesPerChannel[iBranch] = new TH1F*[cNumberOfChannels];
    for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
      histoVariablesPerChannel[iBranch][iChannel] = new TH1F(Form("histo%s_Channel%d",cTreeFloatBranches[iBranch].Data(),iChannel),"",cHistogramsBins[iBranch][kNBins],cHistogramsBins[iBranch][kMinBin],cHistogramsBins[iBranch][kMaxBin]);
      histoVariablesPerChannel[iBranch][iChannel]->Sumw2();
      histoVariablesPerChannel[iBranch][iChannel]->SetTitle(Form("%s distribution in channel %d ",cTreeFloatBranches[iBranch].Data(),iChannel));
      SetHistoStyle(histoVariablesPerChannel[iBranch][iChannel], kBlack, kFullCircle,0.5,cTreeFloatBranches[iBranch] , "counts");
    }
  }

  //Define also an array of 2-D histograms for X and Y:
  TH2D **histoXYPerChannel = new TH2D*[cNumberOfChannels];
  for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
    histoXYPerChannel[iChannel] = new TH2D(Form("histoXY_Channel%d",iChannel),"",cNumberOfXBins,cXBinMin,cXBinMax,cNumberOfYBins,cYBinMin,cYBinMax);
    histoXYPerChannel[iChannel]->Sumw2();
    histoXYPerChannel[iChannel]->SetTitle(Form("XY of tracks in %d ",iChannel));
  }

  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //Define histograms containig event-by-event delta of two channels for one variable
  TH1F **histoDeltaVariables = new TH1F*[cNumberOfDeltaHistograms];
  for(Int_t iDeltaHisto=0;iDeltaHisto<cNumberOfDeltaHistograms;iDeltaHisto++){
    histoDeltaVariables[iDeltaHisto] = new TH1F(Form("histoDelta%s_Channels_%d_%d",cTreeFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]].Data(),arrayDeltaHistograms[iDeltaHisto][1],arrayDeltaHistograms[iDeltaHisto][2]),"",2*cHistogramsBins[arrayDeltaHistograms[iDeltaHisto][0]][kNBins],-1*cHistogramsBins[arrayDeltaHistograms[iDeltaHisto][0]][kMaxBin],cHistogramsBins[arrayDeltaHistograms[iDeltaHisto][0]][kMaxBin]);
    histoDeltaVariables[iDeltaHisto]->Sumw2();
    histoDeltaVariables[iDeltaHisto]->SetTitle(Form("Delta in %s between Channels %d and %d",cTreeFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]].Data(),arrayDeltaHistograms[iDeltaHisto][1],arrayDeltaHistograms[iDeltaHisto][2]));
    SetHistoStyle(histoDeltaVariables[iDeltaHisto], kBlack, kFullCircle,0.5,Form("#delta_{%s}",cTreeFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]].Data()) , "counts");
  }
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//

  //Loop over the tree entries, get the branches and fill the histograms
  for(Long_t iEntry=0;iEntry<numberOfEntries;iEntry++){
    printf("Filling histograms  ... %.0f%%%s", 100.*iEntry/numberOfEntries, (iEntry < numberOfEntries) ? "\r" : "\n");
    testBeamEventTree->GetEntry(iEntry);

    //-------------------------------------------------------------------------------------------------------------------------------------//
    //If verbose option is on, print the branches (use it only for debugging)
    //Do not add this section of code to a useful one. It will be deleted
    if(cVerboseOption){
      cout<<Form("-------------------- Entry %ld --------------------",iEntry)<<endl;
      for(Int_t iBranch=0;iBranch<cNumberOfIntBranches;iBranch++){
        cout<<Form("%s = %d,  ",cTreeIntBranches[iBranch].Data(),arrayIntBranches[iBranch]);
      }
      cout<<"eventTime = "<<eventTime<<endl;

      for(Int_t iBranch=0;iBranch<cNumberOfFloatBranches;iBranch++){
        cout<<Form("%s:",cTreeFloatBranches[iBranch].Data())<<endl;
        for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
          cout<<Form("%s[%d] = %4.4f",cTreeFloatBranches[iBranch].Data(),iChannel,arrayFloatBranches[iBranch][iChannel])<<endl;
        }
      }

    }//end if verbose option
    //-------------------------------------------------------------------------------------------------------------------------------------//

    //-------------------------------------------------------------------------------------------------------------------------------------//
    //Here apply (if asked for) the cut on the delta time between the two requested channels in CommonParams.C
    //Not to mix things up with delta histograms below
    //Calculate the delta between the two channels:
    if(cApplyDeltaCut){
      Double_t deltaToBeCut = arrayFloatBranches[cDeltaCutVariable][cDeltaCutFirstChannel]-arrayFloatBranches[cDeltaCutVariable][cDeltaCutSecondChannel];
      if(TMath::Abs(deltaToBeCut - cutCenterValue) > 1000){
        continue;
      }
    }
    //-------------------------------------------------------------------------------------------------------------------------------------//

    //-------------------------------------------------------------------------------------------------------------------------------------//
    // Applying noise cut. Set the settings for this cut (signal; noise, value) in CommonParams.C
    if(cApplyNoiseCut){
      if(arrayFloatBranches[cNoiseCutVariables[0]][cNoiseCutChannel] < cNoiseCutValue*arrayFloatBranches[cNoiseCutVariables[1]][cNoiseCutChannel]) continue;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------//


    //-------------------------------------------------------------------------------------------------------------------------------------//
    //Fill all simple histograms.
    //TODO: Add something somwhere in order to specify the histograms to be filled. Currently every one is filled
    for(Int_t iBranch=0;iBranch<cNumberOfFloatBranches;iBranch++){
      for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){

	//Add fiducial cuts to study specific regions


	//Select the JTE part of the sensor
	//if(0.0185*arrayDoubleBranches[kXtr][iChannel] < 5.2 ) continue;
	//if((0.0185*arrayDoubleBranches[kXtr][iChannel] > 5.4) && (0.0185*arrayDoubleBranches[kXtr][iChannel] < 6.35) && (0.0185*arrayDoubleBranches[kYtr][iChannel] > 4.6) && (0.0185*arrayDoubleBranches[kYtr][iChannel] < 5.5)) continue;
	//if(0.0185*arrayDoubleBranches[kXtr][iChannel] > 6.6 ) continue;
	//if(0.0185*arrayDoubleBranches[kYtr][iChannel] > 5.7 ) continue;
	//Select the bulk of the sensor
		if((0.0185*arrayDoubleBranches[kXtr][iChannel] < 5.4) && (0.0185*arrayDoubleBranches[kXtr][iChannel] > 6.35) && (0.0185*arrayDoubleBranches[kYtr][iChannel] < 4.6) && (0.0185*arrayDoubleBranches[kYtr][iChannel] > 5.5)) continue;


        histoVariablesPerChannel[iBranch][iChannel]->Fill(arrayFloatBranches[iBranch][iChannel]);
      }
    }
    //-------------------------------------------------------------------------------------------------------------------------------------//

    //-------------------------------------------------------------------------------------------------------------------------------------//
    //Fill XY histograms
      for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
	 if(arrayFloatBranches[kcharge][iChannel] < 2) continue;
        histoXYPerChannel[iChannel]->Fill(0.0185*arrayDoubleBranches[kXtr][iChannel],0.0185*arrayDoubleBranches[kYtr][iChannel]);

      }
    //-------------------------------------------------------------------------------------------------------------------------------------//

    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //Fill delta histograms.
    // cout<<cNumberOfDeltaHistograms<<endl;
    for(Int_t iDeltaHisto=0;iDeltaHisto<cNumberOfDeltaHistograms;iDeltaHisto++){
      Int_t firstChannel = arrayDeltaHistograms[iDeltaHisto][1];
      Int_t secondChannel = arrayDeltaHistograms[iDeltaHisto][2];
      Int_t variableIndex = arrayDeltaHistograms[iDeltaHisto][0];
      TString variableName = cTreeFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]];
      Double_t firstValue = arrayFloatBranches[variableIndex][firstChannel];
      Double_t secondValue = arrayFloatBranches[variableIndex][secondChannel];
      // cout<<Form("Difference in variable %d (%s) between channel %d and %d is %4.4f - %4.4f = %4.4f",variableIndex,variableName.Data(),firstChannel,secondChannel,firstValue,secondValue,firstValue-secondValue)<<endl;
      if(arrayFloatBranches[kpulseHeight][firstChannel] < 10) continue;
      if(arrayFloatBranches[kpulseHeight][secondChannel] < 10) continue;
      histoDeltaVariables[iDeltaHisto]->Fill(arrayFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]][arrayDeltaHistograms[iDeltaHisto][1]]-arrayFloatBranches[arrayDeltaHistograms[iDeltaHisto][0]][arrayDeltaHistograms[iDeltaHisto][2]]);//WTF is this ugliness !!!!!!!!!!!!!!!!
    }
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//
    //*************************************************************************************************************************************//


  }//End loop over tree entries


  //-------------------------------------------------------------------------------------------------------------------------------------//
  //Create and output file and store the histograms:
  //Directory per channel or directory per variable ??
  TString outputFileName = Form("%s/AnalysisResults_",cHistoResultsPath.Data());
  if(cApplyDeltaCut){
    outputFileName.Append(Form("WithDeltaCutOnChannels%dand%d_",cDeltaCutFirstChannel,cDeltaCutSecondChannel));
  }
  if(cApplyNoiseCut){
    outputFileName.Append(Form("WithNoiseCutOnChannel%d_",cNoiseCutChannel));
  }
  outputFileName.Append(Form("%s.root",configName.Data()));

  TFile *outputHistosFile = new TFile(outputFileName,"RECREATE");
  //An array of directories per variable
  TDirectory *arrayOfDirectories[cNumberOfFloatBranches];
  for(Int_t iBranch=0;iBranch<cNumberOfFloatBranches;iBranch++){
    arrayOfDirectories[iBranch] = outputHistosFile->mkdir(Form("%s",cTreeFloatBranches[iBranch].Data()));
    arrayOfDirectories[iBranch]->cd();
    for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
      histoVariablesPerChannel[iBranch][iChannel]->Write();
    }
  }

  TDirectory *directoryXY = outputHistosFile->mkdir("XY");
  directoryXY->cd();
  for(Int_t iChannel=0;iChannel<cNumberOfChannels;iChannel++){
    histoXYPerChannel[iChannel]->Write();
  }
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //Create a directory for delta histograms
  TDirectory *dirDelta = outputHistosFile->mkdir("Delta_Histograms");
  dirDelta->cd();
  for(Int_t iDeltaHisto=0;iDeltaHisto<cNumberOfDeltaHistograms;iDeltaHisto++){
    histoDeltaVariables[iDeltaHisto]->Write();
  }
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//
  //*************************************************************************************************************************************//

  outputHistosFile->Close();
  cout<<Form("\n Done reading the tree for the configuration %s",configName.Data())<<endl;
  //-------------------------------------------------------------------------------------------------------------------------------------//

}
